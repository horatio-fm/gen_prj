"""
content of git hook files (post-checkout, post-commit, etc):

#!/bin/sh
C:\\Python\\3.9.5\\python.exe -m version.version_self

"""


import os
import subprocess
import sys
import getpass
import glob


username = getpass.getuser()

git = "git"

proc = subprocess.Popen('{0} diff --exit-code'.format(git), stdout=subprocess.PIPE, shell=True)
info = proc.communicate()[0].decode("utf-8").rstrip()

if proc.returncode != 0:
    print("There are local changes in the working directory!!")
    print("There are local changes in the working directory!!")
    print("There are local changes in the working directory!!")
    print("There are local changes in the working directory!!")
    print("There are local changes in the working directory!!")
    print("DDD!!")
    sys.exit(1)


proc = subprocess.Popen('{0} describe --tags'.format(git), stdout=subprocess.PIPE, shell=True)
version = proc.communicate()[0].decode("utf-8").rstrip()

proc = subprocess.Popen('{0} rev-parse --short HEAD'.format(git), stdout=subprocess.PIPE, shell=True)
hashkey = proc.communicate()[0].decode("utf-8").rstrip()

proc = subprocess.Popen('{0} rev-parse --git-dir'.format(git), stdout=subprocess.PIPE, shell=True)
git_dir = proc.communicate()[0].decode("utf-8").rstrip()

if len(version.split("-")) > 1:
    version = "-".join(version.split("-")[:-1] + [hashkey])
else:
    version = "-".join([version, hashkey])

proc = subprocess.Popen('{0} rev-parse --abbrev-ref HEAD'.format(git), stdout=subprocess.PIPE, shell=True)
current_branch = proc.communicate()[0].decode("utf-8").rstrip()


proc = subprocess.Popen('{0} show -s --format=%ci HEAD'.format(git), stdout=subprocess.PIPE, shell=True)
time_of_commit = proc.communicate()[0].decode("utf-8").rstrip()
time_of_commit = time_of_commit.replace(" ", "_")
time_of_commit = time_of_commit.replace(":", "-")


version_py_file = {
    # "C:/location/.git/worktrees/dnap3_d_f1": r"C:\location\dnap\version.py",
    ".git": r"C:\for_backup_tch\tch_proj\gen_prj\version\version.py",
}[git_dir]


with open(version_py_file, "w") as fp:
    fp.write("version = '")
    fp.write("__".join([version, time_of_commit, current_branch]))
    # fp.write("__".join([version, time_of_commit.replace(" ", "_"), current_branch]))
    fp.write("'")
    fp.write(f"\nhashkey = '{hashkey}'")

