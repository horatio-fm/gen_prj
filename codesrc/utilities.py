import datetime
import glob
import os
import shutil


def now_time_stamp():
    now = datetime.datetime.now()
    return ("%d-%02d-%02d--%02d-%02d-%02d" % (now.year, now.month, now.day, now.hour, now.minute, now.second))


def output_file_exists(file, out_run_dir, overwrite):

    if overwrite:
        return False

    filename = os.path.basename(file)

    previous_versions = glob.glob(os.path.join(os.path.dirname(out_run_dir), f"**\\*{filename}"), recursive=True)
    if len(previous_versions) == 0:
        return False
    previous_versions.sort()

    shutil.copyfile(previous_versions[-1], file)

    return True
