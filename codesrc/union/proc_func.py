from codesrc.union.load_func import load_inp_df2, load_inp_df1, load_ref_union
from codesrc.union.vars import inp_union_file


def make_union():

    bigdata = load_ref_union()

    df1 = load_inp_df1()
    df2 = load_inp_df2()

    result = df1.append(df2, ignore_index=True)
    result.drop_duplicates(subset=None, keep='first', inplace=True)

    result = result.append(bigdata, ignore_index=True)
    result.drop_duplicates(subset=None, keep='first', inplace=True)
    
    result.to_csv(inp_union_file, sep="\t", index = None)