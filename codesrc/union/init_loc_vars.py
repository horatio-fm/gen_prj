import os
import shutil
from pathlib import Path

from version.version import hashkey, version

from codesrc.utilities import now_time_stamp

this_module_dir = os.path.dirname(os.path.realpath(__file__))
project_dir = [os.fspath(x) for x in Path(this_module_dir).parents if os.fspath(x).endswith("gen_prj")]

if len(project_dir) != 1:
    raise ValueError("len(project_dir) != 1")

project_dir = project_dir[0]

module_name = "gen_union"
module_file = "gen_union.py"

time_stamp = now_time_stamp()

default_input_dir = os.path.join(project_dir, "data", module_name, "inp")
out_run_dir = os.path.join(project_dir, "data", module_name, time_stamp + "__" + hashkey)

input_dir = os.path.join(out_run_dir, "inp")
output_dir = os.path.join(out_run_dir, "out")
out_code_dir = os.path.join(out_run_dir, "codesrc")

# os.makedirs(input_dir, exist_ok=True)
os.makedirs(output_dir, exist_ok=True)

# shutil.copytree(default_input_dir, input_dir, dirs_exist_ok=True)
# shutil.copytree(os.path.dirname(__file__), out_code_dir, dirs_exist_ok=True)
shutil.copytree(default_input_dir, input_dir)
shutil.copytree(os.path.dirname(__file__), out_code_dir)

#########################################################################################################
#########################################################################################################
