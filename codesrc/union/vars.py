import os.path

from .init_loc_vars import *

collapse_thr = 500

ref1 = os.path.join(input_dir, "db", "ref_df1.txt")
ref2 = os.path.join(input_dir, "db", "ref_df2.txt")

inp1 = os.path.join(input_dir, "inp_df1.txt")
inp2 = os.path.join(input_dir, "inp_df2.txt")


ref_union_file = os.path.join(output_dir, "ref_union_df.txt")

inp_union_file = os.path.join(output_dir, "inp_union_df.txt")

