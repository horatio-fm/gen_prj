import pandas as pd


from .vars import *
from codesrc.utilities import output_file_exists


def load_ref_df1():

    df = pd.read_csv(ref1, sep="\t")
    return df


def load_ref_df2():

    df = pd.read_csv(ref2, sep="\t")
    return df


def load_inp_df1():

    df = pd.read_csv(inp1, sep="\t")
    return df


def load_inp_df2():

    df = pd.read_csv(inp2, sep="\t")
    return df


def load_ref_union():

    if output_file_exists(ref_union_file,  out_run_dir, False):
        df = pd.read_csv(ref_union_file, sep="\t")
        return df

    df1 = load_ref_df1()
    df2 = load_ref_df2()

    bigdata = df1.append(df2, ignore_index=True)
    bigdata.drop_duplicates(subset=None, keep='first', inplace=True)

    bigdata.to_csv(ref_union_file, sep="\t", index=None)

    return bigdata
